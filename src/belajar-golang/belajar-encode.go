package main

import "fmt"
import "encoding/json"

type User struct{
	FullName string `json:"Name"`
	Age int
}


func main() {
    var object = []User{{"arief",25},{"manggala",20}}
    var jsonData , err = json.Marshal(object)

    if err !=nil{
    	fmt.Println(err.Error)
    	return
    }

    //var jsonString = string(jsonData)
    fmt.Println(jsonData)
}