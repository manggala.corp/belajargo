package main


import "fmt"
import "net/http"
import "encoding/json"
import "bytes"
import "net/url"

var baseUrl = "http://localhost:9999"


type student struct{
	ID string
	Name string
	Grade int
}
var data = []student{
	student{"E0001","arief",20},
	student{"A0001","manggal",21},
}



func fetchUsers() ([]student,error){
	var err error
	var client  = &http.Client{}
	var data []student

	request,err :=http.NewRequest("POST",baseUrl+"/users",nil)

	if err !=nil{
		return nil,err
	}

	response, err := client.Do(request)
	if err != nil{
		return nil,err
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&data)
	if err != nil{
		return nil, err
	}

	return data,nil

}

func main(){
	var users, err = fetchUsers()
	if err !=nil{
		fmt.Println("Error !",err.Error())
		return
	}

	for _,each := range users{
		fmt.Printf("ID: %s\t Name: %s\t Grade: %d\n",each.ID,each.Name,each.Grade)
	}
}
