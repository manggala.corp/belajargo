package main

import "encoding/json"
import "net/http"
import "fmt"

type student struct{
	ID string
	Name string
	Grade int
}

var data = []student{
	student{"E0001","arief",20},
	student{"A0001","manggal",21},
}


func users(w http.ResponseWriter,r *http.Request){
	w.Header().Set("Content-type","application/json")

	if r.Method == "POST"{
		var result, err = json.Marshal(data)

		if err != nil{
			http.Error(w,err.Error(),http.StatusInternalServerError)
			return
		}

		w.Write(result)
		return
	}


	http.Error(w,"",http.StatusBadRequest)
}

func user(w http.ResponseWriter,r *http.Request){
	w.Header().Set("Content-type","application/json")

	if r.Method == "POST"{
		var id = r.FormValue("id")
		var result  []byte
		var err error

		for _,each := range data{
			if each.ID == id{
				result, err = json.Marshal(each)

				if err !=nil{
					http.Error(w,err.Error(),http.StatusInternalServerError)
					return
				}

				w.Write(result)
				return
			}
		}
		
		http.Error(w,"user not found",http.StatusBadRequest)
		return

	}

	http.Error(w,"",http.StatusBadRequest)
}


func main(){
	http.HandleFunc("/users",users)
	http.HandleFunc("/user",user)

	fmt.Println("starting web server go lang localhost://9999 ")

	http.ListenAndServe(":9999",nil)

}