package main

import "fmt"
import 	"database/sql"
import _"github.com/go-sql-driver/mysql"
import _ "github.com/lib/pq"
import  "time"
import  "os"



type variable struct{
	event_id *string
	vehicle_id int
	date_time *string
	event_type int
	position_id int64
	object_id int
	duration int
	description *string
	event_type_desc *string
	geofence_code *string
	geofence_name *string
	geofence_type *string
	check_point_code *string
	check_point_name *string
	site_code *string
	site_name *string
	site_type *string
	route_code *string
	route_name *string
}

func connect() (*sql.DB,error){
	 db, err :=sql.Open("postgres","user=postgres dbname=gps sslmode=disable")
	//db,err :=sql.Open("mysql","root:123123@tcp(127.0.0.1:3306)/tes")
	if err !=nil{
		return nil,err
	}

	return db,nil
}


func sqlQuery(){
	db,err := connect()

	if err != nil{
		fmt.Println(err.Error())
		return
	}

	defer db.Close()

	
// stmt, telo := db.Prepare(`do $$ begin execute format($f$create table %I()$f$,$1); end; $$;`)

// if telo != nil {
//     	fmt.Println(telo.Error())
// }
// defer stmt.Close()

// _, asem := stmt.Exec(`
// CREATE TABLE IF NOT EXISTS events (
// 	id serial PRIMARY KEY,
// 	type text NOT NULL,
// 	created_at timestamp with time zone DEFAULT current_timestamp
// )`)

// 	if err !=nil{
// 		fmt.Println(asem.Error())
// 		return
// 	}

// event_id
// vehicle_id
// date_time
// event_type
// position_id
// object_id
// duration
// description
// event_type_desc
// geofence_code
// geofence_name
// geofence_type
// check_point_code
// check_point_name
// site_code
// site_name
// site_type
// route_code
// route_name

    t := time.Now().Local()
    s := t.Format("2006")
    fmt.Println(t, "=>", s)




stmt, err := db.Prepare("CREATE TABLE IF NOT EXISTS gps.gps_events_download"+s+"(event_id int8 NOT NULL , "+
	"vehicle_id varchar(10), "+
	"date_time timestamp, "+
	"event_type int8, "+
	"position_id int8, "+
	"object_id int8, "+
	"duration int8, "+
	"description varchar(1000), "+
	"event_type_desc varchar(255), "+
	"geofence_code varchar(255), "+
	"geofence_name varchar(255), "+
	"geofence_type varchar(255), "+
	"check_point_code varchar(255), "+
	"check_point_name varchar(255), "+
	"site_code varchar(255), "+
	"site_name varchar(255), "+
	"site_type varchar(255), "+
	"route_code varchar(255), "+
	"route_name varchar(255), "+
	"PRIMARY KEY (event_id));") 
if err != nil { 
	fmt.Println(err.Error()) 
}
_, err = stmt.Exec() 
if err != nil { 
	fmt.Println(err.Error()) 
} else { 
	fmt.Println("Table created successfully..")
}
defer db.Close()

	//var id = 13
	rows,err := db.Query("select event_id,vehicle_id,date_time,event_type,position_id,duration,description,event_type_desc,geofence_code,geofence_name,geofence_type,check_point_code,check_point_name,site_code,site_name,site_type,route_code,route_name from gps.gps_events_download where   date_time between now() - interval '30 days' and now() ")
	//rows,err := db.Query("select  count(*) as event_type from gps.gps_events_download where date_time between now() - interval '30 days' and now() ")



	if err !=nil{
		fmt.Println("tes")
		return
	}

	defer rows.Close()

	var result []variable

	for rows.Next(){
		var each = variable{}
		var err = rows.Scan(&each.event_id,&each.vehicle_id,&each.date_time,&each.event_type,&each.position_id,&each.duration,&each.description,&each.event_type_desc,&each.geofence_code,&each.geofence_name,&each.geofence_type,&each.check_point_code,&each.check_point_name,&each.site_code,&each.site_name,&each.site_type,&each.route_code,&each.route_name)

		if err != nil{
			fmt.Println(err.Error())
			return
		}
		result = append(result,each)
	}

	

	if err = rows.Err();err != nil{
		fmt.Println(err.Error())
		return 
	}

	// for _,each := range result{
	// 	fmt.Println(each.description)
	// }
	for _,each := range result{
		fmt.Println(each.event_id)
		_,errt := db.Exec("insert into gps.gps_events_download"+s+" values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)",each.event_id,each.vehicle_id,each.date_time,each.event_type,each.position_id,each.object_id,each.duration,each.description,each.event_type_desc,each.geofence_code,each.geofence_name,each.geofence_type,each.check_point_code,each.check_point_name,each.site_code,each.site_name,each.site_type,each.route_code,each.route_name)
		if errt !=nil{
			fmt.Println(errt.Error())
			return
		}

		fmt.Println("insert success")
	}
}

func main(){

	sqlQuery()
}

